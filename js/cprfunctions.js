$(document).ready(function() {

    // For hvert konfigureret CPR nummer (array)
    $.each(cpr_functions['cpr_fields'], function(key, field_group) {
        
        // find feltet i DOM vha feltets variable navn:
        var cpr_field_name = field_group['cprfield'] ;
        var tr = $('tr[sq_id='+cpr_field_name+']');
        var input = $('input:text',tr);
        if( input.length ) {
            // Add event handler to click events
            $(input).on("blur", {'field': cpr_field_name, 'key': key},CPRupdate);
            CPRupdate( {data:{'field': cpr_field_name, 'key': key}} ) ;
        }
    });
});

function CPRupdate(event) {

    // Get the field name and key -> (array id i cpr_fields)
    var field = event.data.field;
   
    var key = event.data.key;
    var cpr_fields = cpr_functions['cpr_fields'][key] ;
        
    // Find the value of the cpr field
    var tr = $('tr[sq_id='+field+']');
    var input = $('input:text',tr);
    var cpr = input.val();
    
    // Clean potential "-" and check that 10 digits remain
    cpr = CPRcheck(cpr);

    // CPRcheck returns '0' if cpr fails check, here we check for '0' and stop script.
    // We let the REDCap CPR validation rule handle the error message to the user.
    if( cpr == '0' ){
		ClearCPRfields(cpr_fields);
        return;
    }
    
    // Extract information from CPR
    var cprVars = CPRcalcs(cpr, cpr_fields) ;
    var dob = cprVars['Day'] + "-" + cprVars['Month'] + "-" + cprVars['Year'] ;
    
    // Tjek at dato er valid, ellers ryd alt
    if( ! isDateValid(dob) ){
        ClearCPRfields(cpr_fields);
        return ;
    }
    
    if( cpr_fields['dobfield'] ) {
        UpdateDobField(cpr_fields['dobfield'], dob) ;
    }
    
    if( cpr_fields['sexfield'] ) {
        UpdateSexField(cpr_fields['sexfield'], cprVars);
    }    
    
    if( cpr_fields['modulusfield'] ) {
        UpdateMod11Field(cpr_fields['modulusfield'], cprVars) ;
    }

}

function UpdateMod11Field( modulusfield, cprVars, clear=false ){
    var tr = $('tr[sq_id="'+modulusfield+'"]');
    if( ! tr.length ) return ;
	if( clear ) {
		$("input[name='"+modulusfield+"___radio']", tr).prop('checked', false) ;
		$("input[name='"+modulusfield+"']", tr).val('') ;
		return ;
	}
	var input = $("input[value='"+cprVars['Modulus11']+"']" , tr) ;
	if( ! input.length ) return ;
	input.prop('checked', true).trigger('click') ;
}

function UpdateDobField(dobfield, dob){
    var tr = $('tr[sq_id="' + dobfield + '"]');
    if( ! tr.length ) return ;
    var input = $('input:text',tr);
    if( ! input.length ) return ;
    input.val(dob).trigger('change') ;
}

function UpdateSexField(sexfield, cprVars, clear=false) {
    var tr = $('tr[sq_id="'+sexfield+'"]');
    if( ! tr.length ) return ;
    var select = $('select',tr);
    if( select.length ) {
        if( clear ) {
			$('option:eq(0)', select).prop('selected', true).trigger('change') ;
		} else {
			var option = $('option[value="'+cprVars['Sex']+'"]', select) ;
			option.prop('selected', true).trigger('change') ;    
		}
    } else {
		if( clear ) {
			$("input[name='"+sexfield+"___radio']", tr).prop('checked', false) ;
			$("input[name='"+sexfield+"']", tr).val('') ;
			return ;
		} 
		var input = $('input[value="'+cprVars['Sex']+'"]' , tr) ;
		if( input.length ) {
			input.prop('checked', true).trigger('click') ;
		}
    }
}


function CPRcheck(cpr) {

    var CPRpattern = /^(([012]\d|3[01])(0\d|1[012])\d{2}-?\d{4})$/
    
    if( ! CPRpattern.test(cpr) ) {
        return '0' ;
    }

    cpr = cpr.replace(/-/i,'');
    return cpr;
}

function ClearCPRfields(cpr_fields) {
	
	if( cpr_fields['modulusfield'] ) {
		UpdateMod11Field( cpr_fields['modulusfield'], cprVars={}, clear=true );
	}
	
	if( cpr_fields['sexfield'] ) {
		UpdateSexField( cpr_fields['sexfield'], cprVars={}, clear=true) ;
	}
	
	if( cpr_fields['dobfield'] ) {
		UpdateDobField( cpr_fields['dobfield'], dob="" ) ;
	}
}

function CPRcalcs(cpr, cpr_fields) {
    
    var day = cpr.substr(0,2) ;
    var month = cpr.substr(2,2) ;
    var year = cpr.substr(4,2) ;
    var syvne = cpr[6] ;
    var sidste = cpr[9] ;
    
    // Extract sex from last digit
    if (parseInt(sidste) % 2 == 0 ){
        var sex = cpr_fields['sex_codes_f'] ;
    } else {
        var sex = cpr_fields['sex_codes_m'] ;
    }
    
    // Extract century based on seventh digit
    switch(syvne) {
        case '0':
        case '1':
        case '2':
        case '3':
            var prefixYear = 19 ;
            break;
        case '4':
        case '9':
            if(year <= 36){
                var prefixYear = 20 ;
            } else {
                var prefixYear = 19 ;
            }
            break;
        case '5':
        case '6':
        case '7':
        case '8':
            if(year <= 36){
                var prefixYear = 20 ;
            } else if (year >= 58) {
                var prefixYear = 18 ;
            }
            break;
        default:
            var prefixYear = 99 ;	
    }

    var yearfull = prefixYear + year ;
    
    // Modulus 11
    mlist = [4,3,2,7,6,5,4,3,2,1] ;
    var modulus = 0 ;

    for( i in cpr ){
        modulus += cpr[i]*mlist[i] ;
    }
    var mcheck = (modulus % 11) == 0 ? 1 : 0 ;

    return {
        "Day":day,
        "Month":month,
        "Year":yearfull,
        "Sex":sex,
        "Modulus11":mcheck
    };
}

function isDateValid(s) {
    var bits = s.split('-');
    var d = new Date(bits[2], bits[1] - 1, bits[0]);
    return d && (d.getMonth() + 1) == bits[1];

}
