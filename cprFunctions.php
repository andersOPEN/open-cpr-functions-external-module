<?php

namespace OPEN\cprFunctions;

use REDCap;

class cprFunctions extends \ExternalModules\AbstractExternalModule {
    
	private $_isSurvey ;
	private $_block = false ;

	function redcap_data_entry_form($project_id, $record, $instrument){
		$this->_isSurvey = false ;
		$cfg = $this->parseModuleSettings();
		$this->setJsSettings($cfg) ;
		$this->includeJs('js/cprfunctions.js');
	}
	
	function redcap_survey_page($project_id, $record, $instrument){
		$this->_isSurvey = true ;
		$cfg = $this->parseModuleSettings($instrument);
		$this->setJsSettings($cfg) ;
		$this->includeJs('js/cprfunctions.js');
	}

	protected function includeJs($file) {
        // Use this function to use your JavaScript files in the frontend
		echo '<script src="' . $this->getUrl($file) . '"></script>';
    }

	protected function setJsSettings($settings) {
		// echo "<div class='yellow'>setJsSettings</div>" ;
        echo '<script>cpr_functions = ' . json_encode($settings) . ';</script>';
    }

	protected function parseModuleSettings($instrument=NULL) {
		
		$settings = array() ;
		$groups = $this->getSubSettings('group_of_fields') ;
		foreach( $groups as $key => $group ){
			if( isset($group['sexfield'])){
				if( ! isset($group['sex_codes_m'])) {
					$groups[$key]['sex_codes_m'] = 'M' ;
				} else {
					$groups[$key]['sex_codes_m'] = $this->escape($groups[$key]['sex_codes_m'] ) ; 
				}
				if( ! isset($group['sex_codes_f'])) {
					$groups[$key]['sex_codes_f'] = 'F' ;
				} else {
					$groups[$key]['sex_codes_f'] = $this->escape($groups[$key]['sex_codes_f'] ) ; 
				}
			}
		}

		$settings['cpr_fields'] = $groups ;
		return $settings ;
	}

}	

?>