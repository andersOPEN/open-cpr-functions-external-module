# REDCap CPR funktioner (external module)

(c) 2020 OPEN // OPEN Patient data Explorative Network

Dette er et "external module" til REDCap, som udregner fødselsdag, køn og modulus 11 fra et eler flere CPR-numre og sætte disse værdier ind i brugerdefinerede felter.

**Udvikler:** Anders Kristian Haakonsson, anders.kristian.haakonsson@rsyd.dk

**Demo:** https://www.iorad.com/player/1702795/REDCap---Automatisk-F-dselsdato-og-k-n-fra-CPR-nummer--OPEN-CPR-functions-#trysteps-1


### LICENSE:

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.